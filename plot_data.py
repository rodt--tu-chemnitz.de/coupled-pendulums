import os
import numpy as np
import json
import sys

import matplotlib.pyplot as plt
import plotparams

def main():
    print('PLOTTING DATA')

    print('reading argv...')

    n_osc = int(sys.argv[1])
    k_inner = float(sys.argv[2]) # connections to wall
    k_outer = float(sys.argv[3]) # connections between oscillators



    print('\ncreating directories...')
    resultsdir = 'results/{0}/'.format(n_osc)
    imgdir = resultsdir + 'img/'

    for dir in [resultsdir, imgdir]:
        print('\t- {0}...'.format(dir), end=' ')
        try:
            os.makedirs(dir)
            print('created!')
        except FileExistsError:
            print('already there!')
            continue
    

    print('reading json...')

    with open('input.json') as f:
        params = json.load(f)

    delta_t = params['delta_t']


    savefile_suffix = '-k_inner={0:.3f}-k_outer={1:.3f}.dat'.format(k_inner, k_outer)
    filename_list = ['results/{0}/data/{1}_data'.format(n_osc, s) + savefile_suffix for s in ['x', 'v']]

    print('plotting...')
    for filename, ylabel, ii in zip(filename_list, [r'$x$ [m]', r'$v$ [m/s]'], [0,1]):
        print('\ttreating {0}'.format(filename))

        print('\t\treading data')
        data = np.loadtxt(filename)
        n_osc, n_timesteps = data.shape
        t_list = np.arange(n_timesteps) * delta_t


        fig, ax = plt.subplots()

        for line, x0 in zip(data, np.arange(n_osc)):
            ax.plot(
                t_list,
                line + x0 + 1.
                )

        ax.set_xlim(t_list[0], t_list[-1])
        ax.set_ylim(0, n_osc+1.0)

        ax.set_xlabel(r'$t$ [s]')
        ax.set_ylabel(ylabel)

        yticks = np.arange(n_osc) + 1
        ax.set_yticks(yticks)
        if ii == 1: # for velocities set yticklabels to zero
            yticklabels = [r'$v_{0}$'.format(jj+1) for jj in np.arange(n_osc)]
            ax.set_yticklabels(yticklabels)
                

        savename = filename.split('/')[3].split('-')[0]
        savename = imgdir + savename + savefile_suffix[:-4] + '.png'
        print('\t\tsaving to {0}'.format(savename))
        fig.savefig(savename)
        plt.close(fig)

if __name__ == '__main__':
    main()