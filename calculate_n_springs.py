import sys
import json
import os
import numpy as np


def main():
    print('CALCULATING MOTION OF COUPLED OSCILLATORS')



    #################################################
    # reading json
    #################################################

    print('reading argv...')

    n_osc = int(sys.argv[1])
    k_inner = float(sys.argv[2]) # connections to own potential
    k_outer = float(sys.argv[3]) # connections between oscillators

    print('\t- {0} ooscillators'.format(n_osc))
    print('\t- k_inner = {0}'.format(k_inner))
    print('\t- k_outer = {0}'.format(k_outer))

    print('reading json...')

    with open('input.json') as f:
        params = json.load(f)

    t_max = params['t_max']
    delta_t = params['delta_t']
    n_timesteps = int(t_max / delta_t)
    m = params['m']

    print('\t- calculating until t={0} width dt={1} -> {2} timesteps'.format(t_max, delta_t, n_timesteps))
    print('\t- mass: m={0}'.format(m))

    # setting mass and spring constants
    m_list = np.array([m for ii in range(n_osc)])
    k_inner_list = np.array([k_inner for ii in range(n_osc)])   # proprietary springs of oscillators 
    k_outer_list = np.array([k_outer for ii in range(n_osc-1)]) # springs connecting oscillators

    # setting starting conditions
    x0_list = np.array([0. for ii in range(n_osc)])
    v0_list = np.array([0. for ii in range(n_osc)])
    # leftmost oscillator is excited
    v0_list[0] = 0.5



    #################################################
    # iterating
    #################################################

    print('iterating...')

    # initialize everything
    x_lastlist = x0_list
    v_lastlist = v0_list

    x_listlist = [x_lastlist]
    v_listlist = [v_lastlist]

    for ii in range(n_timesteps):
        progress_percentage = ((ii+1)/n_timesteps) * 100.
        if progress_percentage % 10. == 0.:
            print('\t{0:.0f} %'.format(progress_percentage))

        x_nextlist = []
        v_nextlist = []

        for jj in range(n_osc):
            # force of own oscillator
            x_C = x_lastlist[jj]
            k_C = k_inner_list[jj]
            force_C = - x_C * k_C
            force_total = force_C

            # force due to osc. to the left
            if jj > 0:
                x_L = x_lastlist[jj-1]
                k_L = k_outer_list[jj-1]
                force_L = (x_L - x_C) * k_L
                force_total = force_total + force_L
            
            # force due to osc. to the right
            if jj < n_osc-1:
                x_R = x_lastlist[jj+1]
                k_R = k_outer_list[jj]
                force_R = (x_R - x_C) * k_R
                force_total = force_total + force_R

            # calculate new velocity
            v_last = v_lastlist[jj]
            v_next = v_last + force_total / m_list[jj] * delta_t
            v_nextlist.append(v_next)

            x_last = x_lastlist[jj]
            x_next = x_last + v_next * delta_t # use v_next here for better numerics
            x_nextlist.append(x_next)

        # prepend results of this timestep
        x_listlist.append(x_nextlist)
        v_listlist.append(v_nextlist)

        # prepare next timestep
        x_lastlist = x_nextlist
        v_lastlist = v_nextlist
    

    #################################################
    # directories
    #################################################

    print('\ncreating directories...')
    resultsdir = 'results/{0}/'.format(n_osc)
    datadir = resultsdir + 'data/'

    for dir in [resultsdir, datadir]:
        print('\t- {0}...'.format(dir), end=' ')
        try:
            os.makedirs(dir)
            print('created!')
        except FileExistsError:
            print('already there!')
            continue

    print('saving...')
    # converting to ndarray for convenience and consistency check
    x_listlist = np.array(x_listlist)
    v_listlist = np.array(v_listlist)

    savefile_suffix = '-k_inner={0:.3f}-k_outer={1:.3f}.dat'.format(k_inner, k_outer)

    np.savetxt(
        datadir + 'x_data' + savefile_suffix,
        x_listlist.T
        )
    
    np.savetxt(
        datadir + 'v_data' + savefile_suffix,
        v_listlist.T
        )

    print('ALL DONE!')
        
            



if __name__ == '__main__':
    main()