set -e

N=$1
k_inner=$2
k_outer=$3

args="$N $k_inner $k_outer"


python calculate_n_springs.py $args

echo
echo

python plot_data.py $args

echo
echo

python calculate_fft.py $args

echo
echo

python plot_fft.py $args