import os
import numpy as np
import sys

import matplotlib.pyplot as plt
import plotparams


def main():
    print('PLOTTING FFT')

    print('\nreading argv...')
    n_osc = int(sys.argv[1])
    print('\t- {0} resonators'.format(n_osc))


    # read data

    print('reading data...')
    resultdir = 'results/{0}/data/'.format(n_osc)

    fft_names = [f for f in os.listdir(resultdir) if f.split('-')[0] == 'x_fft']
    fft_names = sorted(fft_names)

    fft_data = []
    k_aspect_list = []
    for filename in fft_names:
        filename_split = filename[:-4].split('-')
        k_inner = float( filename_split[1].split('=')[1] )
        k_outer = float( filename_split[2].split('=')[1] )

        # save spect -> axis of plot
        k_aspect_list.append(k_outer / k_inner)

        # read fft data
        data = np.loadtxt(resultdir + filename)
        spectrum_leftmost = data[1] # index 1 corresponds to first oscillator
        spectrum_leftmost = spectrum_leftmost / np.linalg.norm(spectrum_leftmost)
        fft_data.append(spectrum_leftmost)

    fft_data = np.array(fft_data) / np.max(fft_data)

    # axis coordinates
    frequency_list = data[0]
    frequency_delta = (frequency_list[1] - frequency_list[0]) * .5

    k_aspect_list = np.array(k_aspect_list)
    k_aspect_delta = (k_aspect_list[1] - k_aspect_list[0]) * .5


    print('plotting...')

    extent = [
        k_aspect_list[0] - k_aspect_delta,
        k_aspect_list[-1] + k_aspect_delta,
        frequency_list[0] - frequency_delta,
        frequency_list[-1] + frequency_delta
        ]


    fig, ax = plt.subplots(figsize=(6,3))

    cb = ax.imshow(
        np.transpose(fft_data),
        origin='lower',
        extent=extent,
        aspect='auto',
        # interpolation='spline36',
        cmap='Blues',
        vmin=0,
        vmax=1
        )

    fig.colorbar(cb, ax=ax, label='normalized fourier coefficient [a.u.]', ticks=[0, 0.5, 1])

    ax.set_xlabel(r'$k_\mathrm{coupling} / k_\mathrm{self}$')

    ax.set_ylabel(r'frequency')

    ax.set_xlim(2,0)
    ax.set_ylim(0.1, 0.275)

    ax.set_xticks([0,0.5,1.0,1.5,2.0])
    ax.set_yticks([0.10, 0.15, 0.20, 0.25])

    fig.savefig('results/{0}/img/fft_contour.png'.format(n_osc))

if __name__ == '__main__':
    main()