import numpy as np
import json
import sys

def main():
    print('CALCULATE FFT')

    
    print('reading argv...')

    n_osc = int(sys.argv[1])
    k_inner = float(sys.argv[2]) # connections between oscillators
    k_outer = float(sys.argv[3]) # connections to wall

    print('\t- {0} oscillators'.format(n_osc))
    print('\t- k_inner = {0}'.format(k_inner))
    print('\t- k_outer = {0}'.format(k_outer))

    savefile_suffix = '-k_inner={0:.3f}-k_outer={1:.3f}.dat'.format(k_inner, k_outer)


    print('\nreading json...')

    with open('input.json') as f:
        params = json.load(f)

    delta_t = params['delta_t']



    print('reading data...')

    loc_data = np.loadtxt('results/{0}/data/x_data'.format(n_osc) + savefile_suffix)

    print('performing FFT...')
    # calculate frequencies
    freqs = np.fft.rfftfreq(loc_data.shape[1], d=delta_t)
    fftresult_list = [freqs]
    for line in loc_data:
        fftresult_now = np.fft.rfft(line)
        fftresult_list.append(np.abs(fftresult_now))
    

    print('saving...')

    np.savetxt(
        'results/{0}/data/x_fft'.format(n_osc) + savefile_suffix, 
        fftresult_list
        )


if __name__ == '__main__':
    main()