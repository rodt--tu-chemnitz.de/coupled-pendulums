import numpy as np
import json
import sys

import matplotlib.pyplot as plt
import plotparams


def main():
    print('PLOTTING FFT')

    print('reading argv...')

    n_osc = int(sys.argv[1])
    k_inner = float(sys.argv[2]) # connections between oscillators
    k_outer = float(sys.argv[3]) # connections to wall

    print('\t- {0} ooscillators'.format(n_osc))
    print('\t- k_inner = {0}'.format(k_inner))
    print('\t- k_outer = {0}'.format(k_outer))

    savefile_suffix = '-k_inner={0:.3f}-k_outer={1:.3f}.dat'.format(k_inner, k_outer)


    print('\nreading data...')
    data = np.loadtxt('results/{0}/data/x_fft'.format(n_osc) + savefile_suffix)
    freqs = data[0]


    print('plotting...')
    fig, ax = plt.subplots()

    for spectrum in data[1:]:
        ax.plot(freqs, spectrum)

    ax.set_xlim(0, 0.5)

    ax.set_xlabel(r'$f$ [$1/\mathrm{s}$]')
    ax.set_ylabel(r'fourier coefficients')

    fig.savefig('results/{0}/img/x_fft'.format(n_osc) + savefile_suffix[:-4] + '.png')

if __name__ == '__main__':
    main()