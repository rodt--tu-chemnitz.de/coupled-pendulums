set -e

echo "VARYING PARAMETERS"

n_res=$1
k_inner=0.5
k_outer_list=$(seq 0.00 0.01 1.00 | tr , .)

echo
echo "n_res = $n_res"
echo "k_inner = $k_inner"
echo

echo "creating directories..."

mkdir -p "results/$n_res/data"
mkdir -p "results/$n_res/img"

echo
echo "iterating..." 
for k_outer in $k_outer_list; do
    echo "    -> k_outer = $k_outer"
    sh execall.sh $n_res $k_inner $k_outer > /dev/null
done

echo
echo "creating FFT contourplot"
python plot_fft_contour.py $n_res > /dev/null
echo
echo "ALL DONE!"
